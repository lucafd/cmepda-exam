Network training Functions
==========================

Functions
---------

.. automodule:: network.train
   :members:
   :undoc-members:
   :show-inheritance:
