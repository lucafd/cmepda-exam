# Cmepda Exam

## Introduction

For the exam, it has been chosen to study the dataset _PlantVillage_[^1]. The dataset used is taken directly from [`kaggle`](https://www.kaggle.com/datasets/abdallahalidev/plantvillage-dataset).

[^1]: J. Arun Pandian; Gopal Geetharamani (2019), “Data for: Identification of Plant Leaf Diseases Using a 9-layer Deep Convolutional Neural Network”, Mendeley Data, V1, doi: 10.17632/tywbtsjrjv.1

## Documentation

The documentation of this project is uploaded to gitlab pages [at this location](https://lucafd.gitlab.io/cmepda-exam/).

## Documents for the presentation

The abstract and the slides are in italian only.
You can find the **abstract** of the project [**here**](https://gitlab.com/lucafd/cmepda-exam/-/jobs/artifacts/main/raw/abstract.pdf?job=compile_pdf): it quickly describes the project and its target.
You can download the **slides** used in the presentation [**here**](https://gitlab.com/lucafd/cmepda-exam/-/jobs/artifacts/main/raw/slides.pdf?job=compile_pdf).

## License

This repository is licensed under the MIT License, that you can consult on the `LICENSE` file in the repository.

___

[![pipeline](https://gitlab.com/lucafd/cmepda-exam/badges/main/pipeline.svg)](https://gitlab.com/lucafd/cmepda-exam/)