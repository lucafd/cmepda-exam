import numpy as np
import matplotlib.pyplot as plt

# qua ecco
epoch, train_loss, train_acc, val_loss, val_acc = np.loadtxt('./logRGB.txt', unpack=True)

plt.plot(epoch, train_loss, color='red', label='Training')
plt.plot(epoch, val_loss, color='blue', label='Validation')
plt.legend(loc='best')
plt.xlabel("Epochs")
plt.ylabel("Loss")
plt.savefig("rgb-loss.pdf")

plt.clf()

plt.plot(epoch, train_acc, color='red', label='Training')
plt.plot(epoch, val_acc, color='blue', label='Validation')
plt.legend(loc='best')
plt.xlabel("Epochs")
plt.ylabel("Accuracy")
plt.savefig("rgb-accuracy.pdf")