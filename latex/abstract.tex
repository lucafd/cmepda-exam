\documentclass[a4paper]{article}
% formatting
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{graphicx} % Required for inserting images
\usepackage{hyperref}
\usepackage{subcaption}
\usepackage{cleveref}
\usepackage{indentfirst} % just aesthetics
% physics
\usepackage{siunitx}
\usepackage{derivative} % not used here
% bibliography
\usepackage[autostyle]{csquotes}
\usepackage[style=numeric, sorting=ydnt, maxbibnames=2]{biblatex}
\addbibresource{biblio.bib}
%%%% bibliography formatting
\AtEveryBibitem{\clearfield{abstract}}

\title{
    \textbf{\textsc{CMEPDA -- Progetto di Esame}} \\
    \Large Uno studio sul dataset \textit{PlantVillage}
}
\author{
    Luca Francesco D'Alessandro\footnote{Matricola: \texttt{586649}. Contatto e-mail: \href{mailto:l.dalessandro7@studenti.unipi.it}{l.dalessandro7@studenti.unipi.it}. 
    }
}
\date{Ottobre 2023\footnote{Ultima compilazione: \today.}}

%%%%%%%% begin document
\begin{document}
\maketitle

\section*{Sommario}
%%%% first section

L'obiettivo del progetto scelto per l'esame è quello di realizzare una rete neurale che identifichi la presenza di una malattia sulla foglia di una pianta a partire da una foto della stessa.
Il \textit{repository} del progetto è consultabile al link \url{https://gitlab.com/lucafd/cmepda-exam}, dove è possibile scaricare la versione più aggiornata di questo sommario e dei lucidi per la presentazione.

Prendendo ispirazione da numerosi esempi presenti in letteratura (confronta \cite{mohantyUsing2016,mrishoAccuracy2020}), il progetto si presta ad analizzare il dataset comunemente chiamato \textit{PlantVillage}. 
Questo si compone di \num{87867} immagini \textsc{rgb} di dimensione \texttt{256x256x3}, raffiguranti foglie di varie piante di interesse commerciale su sfondo neutro. 
Le immagini sono state suddivise in tre sottoinsiemi di training, validation e test.
I dataset di training e validation sono in rapporto \qty{80}{\percent}/\qty{20}{\percent} e in entrambi si è avuto cura di mantenere identica la percentuale di piante sane sul numero di piante malate da analizzare, ovvero il \qty{24.1}{\percent}.
Il dataset impiegato per la realizzazione di questa rete è stato scelto per la sua qualità oltre che per il suo intenso studio in letteratura, dato che la distribuzione delle immagini è ben bilanciata fra le classi, come si vede in \cref{fig:barplot-class}, e le immagini stesse non presentano difetti significativi per l'analisi. 

Per realizzare l'obiettivo del progetto, si è allenato una rete neurale a classificare le immagini secondo il tipo di pianta e malattia mostrata dalla foglia. 
L'architettura della rete neurale è stata scelta in una versione semplificata di una \textit{Redisual Network}, similmente a quello suggerito per la prima volta in \cite{he2015deep}: tale architettura, a parità di risorse computazionali, consente di realizzare il training di una rete con tanti livelli in convoluzione e raggiungendo una maggiore accuratezza con profondità elevate.
In questo caso il modello implementato si fonda su una architettura di base ripetuta per tre volte e composta dai seguenti blocchi:
\begin{enumerate}
\item un layer di normalizzazione della batch, in cui ogni neurone riscala linearmente il proprio input in modo che esso abbia media nulla e varianza \num{1} al variare degli elementi della batch;
\item un layer di convoluzione con \num{32} canali in entrata e in uscita, con kernel di taglia \texttt{3x3}, stride \num{1} e padding \num{1};
\item un layer di attivazione ReLU;
\item un layer di convoluzione con \num{32} canali in entrata e in uscita, con kernel di taglia \texttt{3x3}, stride \num{1} e padding \num{1};
\item un layer di normalizzazione della batch;
\item una connessione residuale, che somma all'output del precedente layer l'input dell'intero blocco: in questo modo, in fase di backpropagation, il gradiente avrà norma vicina a \num{1}, cosa che contribuisce a velocizzare la convergenza del modello;
\item un layer di attivazione ReLU.
\end{enumerate}

\begin{figure}[hb]
	\includegraphics[width=\textwidth]{./images/image_per_class_barplot.png}
	\caption{\footnotesize Istogramma del numero di immagini, suddivise per classe. Due classi corrispondenti alla stessa pianta hanno lo stesso colore.}
	\label{fig:barplot-class}
\end{figure}

Nella realizzazione della rete si è scelto di implementare due modelli che in fase di training usano spazi colore diversi per rappresentare le immagini. 
Infatti, è possibile mostrare come le tre coordinate \textsc{rgb} di un immagine siano altamente correlate, confronta \cite{RGBcorrelate}. 
Si è studiato quindi un modello di rete basato sullo spazio colore \texttt{cielab}, che si pone l'obiettivo di approssimare meglio la visione umana. 
Esso è dotato di tre coordinate, la coordinata \texttt{L} che mappa la luminosità della foto, mentre le coordinate \texttt{A} e \texttt{B} mettono su assi ortogonali i colori base della visione umana in coppie di opposti, verde-magenta e blu-giallo.
Questa scelta non è casuale, ma è stata stimolata dalla lettura dell'articolo \cite{schwarzColoraware2022}, dove in effetti si evidenzia un sostanziale miglioramento delle performance della rete, al costo di aumentare il numero di parametri della stessa.
Infatti, nel modello proposto i canali \texttt{L} e \texttt{AB} vengono passati a due rami separati dell'architettura, che successivamente si ricongiungono e passano attraverso gli stessi livelli del primo modello.
La struttura base dei due rami si compone di un blocco base di layer ripetuto tre volte:
\begin{enumerate}
\item un layer di convoluzione, con kernel di taglia \texttt{3x3} e stride variabile;
\item un layer di normalizzazione della batch;
\item un layer di attivazione ReLU.
\end{enumerate}
La conseguenza dell'utilizzo di questa struttura è quella di rendere il risultato del training maggiormente indipendente dalla luminosità dell'immagine e quanto più vicino possibile alla visione dell'occhio umano.
 
Alla fine del progetto, si confronteranno i risultati ottenuti sui due modelli e l'accuratezza finale della rete per l'individuazione delle malattie. 
Nonostante vada ben oltre lo scopo del corso e non sia qui realizzato, il lavoro svolto in questo progetto è evidentemente utilizzabile ed estendibile a realizzazioni pratiche.
Sulla base di questa rete sarebbe infatti possibile pensare di realizzare applicativi per dispositivi mobili, tali da essere sia di supporto ai professionisti sia in grado fornire alla rete maggiori dati su cui poter effettuare il training per migliorarne le performance.


%%%% bibliography
\nocite{*}
\printbibliography
%%%%
\end{document}