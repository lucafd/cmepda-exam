# -*- coding: utf-8 -*-
"""
Definition of the neural network functions used to analyze the PlantVillage dataset.
Function defined are:
- train
- evaluate, accuracy, testing.
Here there is also the function that manages the device used for training.
"""
import os
from datetime import datetime
from tqdm import tqdm
# managing data
import numpy as np
import matplotlib.pyplot as plt
# neural network libraries
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
# libraries to manage images
from PIL import Image
import torch.nn.functional as F
import torchvision.transforms as transforms
from torchvision.datasets import ImageFolder
# library of the model
from network.model import ModelLAB
from network.model import ModelRGB
# this is needed to convert to lab
import kornia

# logging function, needed to print 
def log(msg: str):
    """ Logging function, used for saving the informations about the performances of the network in the log and printing on screen, formatted correctly.

    Input
    -----
    msg: (str) string to be printed and added to log.
    """
    with open('log', 'a') as f:
        f.write(msg + '\n')
    print(msg)

#
def train(Model, channels,  train_dl, valid_dl, device, hyperparameter_x=16):
    """ Train function. Optimizer: used Adam optimizer for the gradient descent optimization algorithm.

    Input
    -----
    Model: the model of the network chosen, use ModelRGB or ModelLAB.
    channels: number of channels "in the center" of the network.
    train_dl: data loaded on the device from the training folder.
    valid_dl: data loaded on the device from the validation folder.
    device: the torch-device onto which the model retrives the data.
    hyperparameter_x: hyperparameter needed in the LAB setting but not in the RGB case, set at 16 as default value, so that the 25% of channels are in the L path.

    Output
    ------
    model: the actual trained model and add to log and print the values

    Write on the log
    """
    torch.cuda.empty_cache()
    history = []

    # hyperparameters
    max_epochs   = 24
    max_lr       = 1e-3
    grad_clip    = 0.05
    weight_decay = 1e-4

    model = Model(channels, hyperparameter_x).to(device)

    optimizer = torch.optim.Adam(model.parameters(), max_lr, weight_decay=weight_decay)
    scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, max_lr, epochs=max_epochs, steps_per_epoch=len(train_dl))

    starting_time = datetime.now()

    # actual training
    for epoch in range(max_epochs):
        print(f'Starting epoch {epoch}')
        model.train()
        train_losses = []
        lr_history   = []

        while len(tqdm._instances) > 0:
            tqdm._instances.pop().close()
        bar = tqdm(total=len(train_dl))

        for batch in train_dl:
            # definition of the loss function, it uses the CrossEntropy function
            images, labels = batch
            out  = model(images)
            loss = F.cross_entropy(out, labels)
            loss.backward()
            train_losses.append(loss)
            # gradient clipping, so that it doesn't explode or disappear
            nn.utils.clip_grad_value_(model.parameters(), grad_clip)

            optimizer.step()
            optimizer.zero_grad()

            lr_history.append(optimizer.param_groups[0]['lr'])
            scheduler.step()

            bar.update(1)
        bar.close()

        # printing and formatting the messages for the stdout and log
        print(f'Starting evaluation of epoch {epoch}')
        train_mean_loss    = torch.stack(train_losses).mean()
        validation_results = evaluate(model, valid_dl)
        val_mean_loss      = validation_results['eval_mean_loss']
        val_mean_accuracy  = validation_results['eval_mean_accuracy']

        time = datetime.now() - starting_time
        
        msg = ''
        msg += f'epoch {epoch}  --  ' 
        msg += f'time: {time}  --  '
        msg += f'train_loss: {train_mean_loss:.4f} - '
        msg += f'val_loss: {val_mean_loss:.4f} - '
        msg += f'val_accuracy: {val_mean_accuracy:.4f}'
        log(msg)

    return {
        'model': model,
    }

#
def evaluate(model, dataset):
    """ Evaluate model on the validation dataset.
    
    Parameters
    ----------
    model: use actual model in the train function.
    dataset: the data onto which to make the evaluation.
    """
    model.eval()
    with torch.no_grad():
        losses     = []
        accuracies = []

        for batch in dataset:
            images, labels = batch
            out      = model(images)
            loss     = F.cross_entropy(out, labels)
            accuracy = compute_accuracy(out, labels)

            losses.append(loss)
            accuracies.append(accuracy)

        mean_loss     = torch.stack(losses).mean()
        mean_accuracy = torch.stack(accuracies).mean()
    return {
        'eval_mean_loss':     mean_loss,
        'eval_mean_accuracy': mean_accuracy
    }

# for calculating the accuracy
def compute_accuracy(outputs, labels):
    _, preds = torch.max(outputs, dim=1)
    return torch.tensor(torch.sum(preds == labels).item() / len(preds))
   
#
def test(model):
    test_dir = "../data/New Plant Diseases Dataset(Augmented)/test"
    test = ImageFolder(test_dir, transform=transforms.ToTensor())
    test_images = sorted(os.listdir(test_dir + '/test')) # since images in test folder are in alphabetical order
    test_dl = DataLoader(test, batch_size, num_workers=4, pin_memory=True)

    results = evaluate(model, test_dl)
    mean_loss     = results['val_mean_loss']
    mean_accuracy = results['val_mean_accuracy']

    log(f'test_mean_loss: {mean_loss:.4f}  -- test_mean_accuracy: {mean_accuracy:.4f}')

# for moving data to device (CPU or GPU)
def to_device(data, device):
    """Move tensor(s) to chosen device. Function needed to create  DeviceDataLoader().

    Parameters
    ----
    data : tensor-like the data you want to move to the device, it doesn't require a specific type, just tensors
    device: torch-device. The device on which the computation has to be made
    """
    if isinstance(data, (list,tuple)):
        return [to_device(x, device) for x in data]
    return data.to(device, non_blocking=True)


# for loading in the device (GPU if available else CPU)
class DeviceDataLoader():
    """Wrap a dataloader to move data to a device.
    
    Parameters
    ------
    dl: data to be loaded
    device: (torch-device) device to load
    """
    def __init__(self, dl, device):
        self.dl = dl
        self.device = device
        
    def __iter__(self):
        """Yield a batch of data after moving it to device"""
        for b in self.dl:
            yield to_device(b, self.device)
        
    def __len__(self):
        """Number of batches.
        """
        return len(self.dl)

################################################ main

if __name__ == "__main__":
    
    while len(tqdm._instances) > 0:
        tqdm._instances.pop().close()
    
    # import data, use os path
    project_dir = os.path.abspath('')
    data_dir = project_dir + "/dataset_with_augmentation/"
    train_dir = data_dir + "/train"
    valid_dir = data_dir + "/valid"

    # some import
    num_diseases = 38

    # custom transform for LAB
    custom_transform = transforms.Compose([
        transforms.ToTensor(),
        kornia.color.rgb_to_lab,
    ])

    # TODO ImageFolder wrap it around so that I have to pass just the directory
    # datasets for validation and training
    train_data = ImageFolder(train_dir, transform=custom_transform)
    valid_data = ImageFolder(valid_dir, transform=custom_transform) 

    # setting the batch size
    batch_size = 32

    # Setting the seed value
    random_seed = 42
    torch.manual_seed(random_seed)

    # DataLoaders for training and validation
    train_dl = DataLoader(train_data, batch_size, shuffle=True, num_workers=6, pin_memory=True)
    valid_dl = DataLoader(valid_data, batch_size,               num_workers=6, pin_memory=True)

    # Device 
    device = torch.device('cuda' if torch.cuda.is_available else 'cpu')

    # Moving data into GPU
    train_dl = DeviceDataLoader(train_dl, device)
    valid_dl = DeviceDataLoader(valid_dl, device)

    # training phase
    train_results = train(ModelRGB, 64, train_dl, valid_dl, device, hyperparameter_x=16)
    model = train_results['model']
