# -*- coding: utf-8 -*-
"""
Definition of the single block components of the neural network model used to analyze the PlantVillage Dataset.
"""
import torch
import torch.nn as nn

# manually defined, it comes from the analysis of the folder of the dataset
num_diseases = 38

### basic block of the network
class ResidualBlock(nn.Module):
    """
    ResidualBlock is a class built on the Torch NN Module class that creates the base structure of the network; it is used repeatedly in ModelRGB and ModelLAB.
    Being a residual network, the forward performs a sum of the output of the last layer with the input.
    
    Parameters
    ------    
    in_channels: int, number of channels in input
    out_channels: int, number of channels in output

    Hyperparameters, inherited from torch.nn.Module, specified for the network
    ------
    kernel_size: 3
    stride: 1
    padding: 1
    activation function: torch.nn.ReLU()

    """
    def __init__(self, in_channels, out_channels):
        """ Constructor.
        """
        super().__init__()
        self.pipe = nn.Sequential(
            nn.BatchNorm2d(in_channels),
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(out_channels),
        )
        self.activation = nn.ReLU() 
        
    def forward(self, x):
        """ Perform the forward of the network to the next block.
        It sums the output of the last layer of the pipe to the input and it performs the normalization with the activation function.
        """
        out = self.pipe(x)
        out = out + x
        out = self.activation(out)
        return out

### RGB model, simple and effective

class ModelRGB(nn.Module):
    """
    Model of the Network for the RGB encoding of the images.
    
    The number of layers before the actual sequence of ResidualBlocks are:
      - Convolution: in_channels=3, out_channels=8, kernel_size=3, stride=1, padding=1
      - Max Pooling: kernel_size=4
      - ReLU
      - Batch Normalization: num_features=8
      - Convolution: in_channels=8, out_channels=channels, kernel_size=3, stride=1, padding=1
      - Max Pooling: num_feautres=2
      - ReLU
    
    The layers after the Residual Blocks are: 
      - Convolution: in_channels=channels, out_channels=16, kernel_size=1, stride=1, padding=0
      - Max Pooling: kernel_size = 2
      - Flattening layer
      - Linear layer: in_features = 16*16*16, out_features = num_diseases
    
    Parameters
    ------
    channels: (int) number of channels used to perform the analysis in the Residual Block.
    hyperparameter_x: d

    Hyperparameters
    ------
    num_diseases: it is defined as the number of classes that appear in the folder.
    """
    def __init__(self, channels, hyperparameter_x):
        """ Constructor, defines the layer of the dimension reduction part and hte 
        """
        # line added to support the train function syntax, so this variable is not needed
        trash = hyperparameter_x
        super().__init__()
        layers = [
            nn.Conv2d(in_channels=3, out_channels=8, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(4),
            nn.ReLU(),
            nn.BatchNorm2d(8),
            nn.Conv2d(in_channels=8, out_channels=channels, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(2),
            nn.ReLU(),
        ]
        for _ in range(5):
            layers.append(ResidualBlock(channels, channels))
        final = nn.Sequential(
            nn.Conv2d(in_channels=channels, out_channels=16, kernel_size=1, stride=1, padding=0),
            nn.MaxPool2d(2),
            nn.Flatten(),
            nn.Linear(16*16*16, num_diseases)
        )
        layers.append(final)
        self.layers = nn.Sequential(*layers)

    def forward(self, x):
        """ Forward function for the RGB model, it returns the layers defined in the constructor.
        """
        return self.layers(x)

### LAB adaptation of the model

class Basic_Convolution(nn.Module):
    """
    It performs the basic convolution block needed in the Network.
    The target of this layer is to reduce the dimensionality of the images to fasten the training time.

    Parameters
    ------
    layer: (torch.nn.Conv2D) layers from the previous block
    out_channels: (int) number of channels in the outputs
    
    """
    def __init__(self, layer, out_channels):
        """ Constructor.
        """
        super().__init__()
        self.conv = layer
        self.normalization = nn.BatchNorm2d(out_channels)
        self.activation = nn.ReLU()

    def forward(self, x):
        """ Forward function.
        """
        t = self.conv(x)
        t = self.normalization(t)
        return self.activation(t)

# path L
class Path_L_Block(nn.Module):
    """ Path L block. It defines the block of the L channel path, and it takes as input the number of output channels that the model should have for this path.

    Parameters
    ------
    hyperparameter_x: (0 < int < 64) it defines the number of output channels for each pixel, thus the weight of the Path_L in the training of the network.

    Hyperparameters, inherited from torch.nn.Module, specified for the network
    ------
    kernel_size: 3
    stride: 1, except for the first convolution, that has a stride of 2
    padding: 1, except for the first convolution, that has no padding 
    Pooling: 2
    """
    def __init__(self, hyperparameter_x):
        """ Constructor. It defines the convolution layers and the pooling layer.
        """
        super().__init__()
        out_channels = hyperparameter_x // 2
        self.conv1   = Basic_Convolution(nn.Conv2d(in_channels=1, out_channels=out_channels, kernel_size=3, stride=2, padding=0), out_channels)
        self.conv2   = Basic_Convolution(nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1), out_channels)
        self.conv3   = Basic_Convolution(nn.Conv2d(in_channels=out_channels, out_channels=hyperparameter_x, kernel_size=3, stride=1, padding=1), hyperparameter_x)
        self.pooling = nn.MaxPool2d(2)

    def forward(self, x):
        """ Forward function, performs the convolutions of the block and then the resize of the image with the MaxPool.
        """
        t = self.conv1(x)
        t = self.conv2(t)
        t = self.conv3(t)
        t = self.pooling(t)
        return t

# Path AB
class Path_AB_Block(nn.Module):
    """ Path AB block. It defines the block of the AB channel path, and it takes as input the number of output channels that the model should have for the other path.

    Parameters
    ------
    hyperparameter_x: (0 < int < 64) it defines the number of output channels for each pixel, that is the weight of the Path_L in the training of the network. The weight of the AB path is 64 - hyperparameter_x.

    Hyperparameters, inherited from torch.nn.Module, specified for the network
    ------
    kernel_size: 3
    stride: 1, except for the first convolution, that has a stride of 2
    padding: 1, except for the first convolution, that has no padding 
    Pooling: 2
    """
    def __init__(self, hyperparameter_x):
        """ Constructor. It defines the convolution layers and the pooling layer.
        """
        super().__init__()
        out_channels = 32 - hyperparameter_x // 2
        self.conv1   = Basic_Convolution(nn.Conv2d(in_channels=2, out_channels=out_channels, kernel_size=3, stride=2, padding=0), out_channels)
        self.conv2   = Basic_Convolution(nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1), out_channels)
        self.conv3   = Basic_Convolution(nn.Conv2d(in_channels=out_channels, out_channels=64 - hyperparameter_x, kernel_size=3, stride=1, padding=1), 64 - hyperparameter_x)
        self.pooling = nn.MaxPool2d(2)

    def forward(self, x):
        """ Forward function, performs the convolutions of the block and then the resize of the image with the MaxPool.
        """
        t = self.conv1(x)
        t = self.conv2(t)
        t = self.conv3(t)
        t = self.pooling(t)
        return t

# Actual Model
class ModelLAB(nn.Module):
    """
    Model of the Network for the LAB encoding of the images.
    
    The number of layers before the actual sequence of ResidualBlocks are:
      - Convolution: in_channels=3, out_channels=8, kernel_size=3, stride=1, padding=1
      - Max Pooling: kernel_size=4
      - ReLU
      - Batch Normalization: num_features=8
      - Convolution: in_channels=8, out_channels=channels, kernel_size=3, stride=1, padding=1
      - Max Pooling: num_feautres=2
      - ReLU
    
    The layers after the Residual Blocks are: 
      - Convolution: in_channels=channels, out_channels=16, kernel_size=1, stride=1, padding=0
      - Max Pooling: kernel_size = 2
      - Flattening layer
      - Linear layer: in_features = 16*16*16, out_features = num_diseases
    
    Parameters
    ------
    channels: (int) number of channels used to perform the analysis in the Residual Block.
    hyperparameter_x: 

    Hyperparameters
    ------
    num_diseases: it is defined as the number of classes that appear in the folder.
    """

    def __init__(self, channels, hyperparameter_x):
        """ Constructor.
        """
        super().__init__()
        self.path_l  = Path_L_Block(hyperparameter_x)
        self.path_ab = Path_AB_Block(hyperparameter_x)

        layers = []
        for _ in range(5):
            layers.append(ResidualBlock(channels, channels))
        final = nn.Sequential(
            nn.Conv2d(in_channels=channels, out_channels=16, kernel_size=1, stride=1, padding=0),
            nn.MaxPool2d(2),
            nn.Flatten(),
            nn.Linear(15376, num_diseases)
        )
        layers.append(final)
        self.layers = nn.Sequential(*layers)

    def forward(self, x):
        """ Forward function, handles the split and the L and AB channel subdivision and merging.
        """
        l_channel, ab_channel = torch.split(x, [1,2], dim=1)
        l_path = self.path_l(l_channel)
        ab_path = self.path_ab(ab_channel)
        concat = torch.cat((l_path, ab_path), dim=1)
        return self.layers(concat)